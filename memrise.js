// ==UserScript==
// @name        MemBotRise
// @namespace   Violentmonkey Scripts
// @match       https://app.memrise.com/course/430131/3a-s2-toeic-vocabulary/5/garden/classic_review/
// @grant       none
// @version     0.0
// @author      -_-/
// ==/UserScript==


var theCode = function () {

    function with_delay() {

        function traduce(word) {
            switch (word) {
                case 'carte': return 'menu';
                case 'à volonté': return 'all you can eat';
                case 'Un apéritif': return 'cocktail';
                case 'Une entrée': return 'appetizer';
                case 'Un plat': return 'dish';
                case 'Un assaisonnement': return 'dressing';
                case 'Un dessert': return 'dessert';
                case 'serveur': return 'waiter';
                case 'serveuse': return 'waitress';
                case 'Un repas équilibré': return 'balanced meal';
                case 'L\'addition': return 'check';
                case 'un pourboire': return 'tip';
                case 'laisser un pourboire': return 'to tip';
                case 'Un chef cuisinier': return 'chef';
                case 'Une soupe': return 'soup';
                case 'Un petit pain': return 'roll';
                case 'La restauration rapide': return 'fast food';
                case 'La malbouffe': return 'junk food';
                case 'l\'eau du robinet': return 'tap water';
                case 'l\'eau pétillante': return 'sparkling water';
                case 'Les couverts': return 'cutlery';
                case 'Une nappe': return 'tablecloth';
                case 'Une serviette de table': return 'napkin';
                case 'Une tasse': return 'cup';
                case 'Une assiette': return 'plate';
                case 'Un verre': return 'glass';
                case 'Du vin': return 'wine';
                case 'commander': return 'to order';
                case 'Une boisson': return 'beverage';
                case 'La porcelaine': return 'china';
                case 'Haché': return 'chopped';
                case 'débarrasser la table': return 'to clear the table';
                case 'les couverts': return 'cutlery';
                case 'Un parfum / arôme': return 'flavour';
                case 'Un four': return 'oven';
                case 'Une casserole': return 'pan';
                case 'Une recette': return 'recipe';
                case 'verser': return 'to pour';
                case 'bio': return 'organic';
                case 'cuire (au four)': return 'to bake';
                case 'Épicé': return 'spicy';
                case 'Une tranche': return 'slice';
                case 'à emporter': return 'to take-away';
                case '24h/24h, 7j/7': return '24/7';
                case '24 heures sur 24': return 'around the clock';
                case 'gratuit': return 'complimentary';
                case 'un balai': return 'broom';
                case 'un évier/un lavabo': return 'sink';
                case 'une gazinière': return 'stove';
                case 'un plateau': return 'tray';
                case 'une casserole': return 'pot';
                case 'un pichet': return 'pitcher';
                default:
                    console.log('Unknown word: \'' + word + '\'');
            }

            return undefined;
        }

        function loop() {

            if (document.querySelector('#boxes > div > button > span') != undefined) {
                // location.reload(false);
                window.location = window.location.href;
                return;
            }

            /* End of the test */
            if (document.querySelector('#boxes > div > div.typing-wrapper > input') == undefined) {
                document.querySelector('#boxes > div > div.next_btn.btn.btn-primary.btn-large > span.text').click();

                // location.reload(false);
                window.location = window.location.href;
                return;
            }

            let word_to_traduce = document.querySelector('#prompt-row > div > div').innerHTML;
            word_to_traduce = word_to_traduce.trim()
            console.log(word_to_traduce, traduce(word_to_traduce));


            document.querySelector('#boxes > div > div.typing-wrapper > input').value = traduce(word_to_traduce);

            document.querySelector('#prompt-area > button > span').click();
            document.querySelector('#boxes > div > button > span').click();
        }

        setInterval(loop, Math.random() * 1000 + 1000);
    }

    setTimeout(with_delay, 1500);
}


var injectCode = function (f) {
    var script = document.createElement('script');
    script.textContent = '(' + f.toString() + '());';
    document.head.appendChild(script);
};

injectCode(theCode);